# What's this?

This is the infrastructure needed for the web of the esLibre congress

It's based on:

* Jekyll, as static site generator, to take advantage of GitLab features for this
* Bootstrap 4.0 for CSS and styling

# How to contribute?

Clone the repo locally and make the changes needed.

To test it, run:
```
$ bundle install
```

One required `gem`s are installed, you can:
```
$ bundle exec jekyll serve
```

And visit `http://localhost:4000` to see how it looks like